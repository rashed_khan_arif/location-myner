package mobile.location.locationminer;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("api/user-relation")
    Observable<List<UserLocation>> getUserByEmailId(@Query("email") String email);
}
