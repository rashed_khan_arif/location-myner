package mobile.location.locationminer;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.microsoft.signalr.Action2;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;
import com.microsoft.signalr.OnClosedCallback;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String SOCKET_URL = LMHttpClient.BASE_URL + "/location-hub";
    private static final int REQ_LOC = 1000;
    HubConnection connection;
    private FusedLocationProviderClient mFusedLocationClient;
    private UserLocation userLocation;
    private LocationCallback mLocationCallback;
    private LocationRequest locationRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(0.5f);

    }

    @SuppressLint("CheckResult")
    public void requestForUserLocation(View view) {
        EditText editText = findViewById(R.id.etEmailAddress);
        String email = editText.getText().toString();
        if (email.isEmpty()) {
            editText.setError("Required");
            return;
        }
        ApiService service = LMHttpClient.getService(ApiService.class);
        service.getUserByEmailId(email).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<UserLocation>>() {
            @Override
            public void accept(List<UserLocation> userLocations) throws Exception {
                userLocation = userLocations.get(0);
                createSocketConnection();
                getLocation();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Toast.makeText(getApplicationContext(), "You are not registered ", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Please grant your location permission", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQ_LOC);
            return;
        }
        if (mFusedLocationClient == null) return;
        initLocationCallback();
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, getMainLooper());
        findViewById(R.id.llContainer).setVisibility(View.GONE);
        findViewById(R.id.tvMsg).setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQ_LOC) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                getLocation();
            } else {
                Toast.makeText(getApplicationContext(), "You must grant location permission !", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (connection != null) {
            if (connection.getConnectionState() == HubConnectionState.CONNECTED) {
                connection.stop();
            }
            findViewById(R.id.llContainer).setVisibility(View.VISIBLE);
            findViewById(R.id.tvMsg).setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "Disconnected from server", Toast.LENGTH_LONG).show();
        }
    }

    private void initLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                sendLocation(locationResult.getLastLocation());
            }
        };
    }


    private void sendLocation(Location location) {
        try {
            if (location != null) {
                if (connection == null) return;
                LMLocation lm = new LMLocation();
                lm.setLat(location.getLatitude());
                lm.setLng(location.getLongitude());
                lm.setUserRelationId(userLocation.getId());
                connection.send("SendMessage", userLocation.getUserId(), new Gson().toJson(lm));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createSocketConnection() {
        connection = HubConnectionBuilder.create(SOCKET_URL).build();
        connection.start();
        connection.onClosed(new OnClosedCallback() {
            @Override
            public void invoke(Exception exception) {
                findViewById(R.id.llContainer).setVisibility(View.VISIBLE);
                findViewById(R.id.tvMsg).setVisibility(View.GONE);
            }
        });
        connection.on("ReceiveMessage", () -> {

        });

        connection.on("ReceiveMessage", new Action2<String, LMLocation>() {
            @Override
            public void invoke(String param1, LMLocation param2) {

            }
        }, String.class, LMLocation.class);

    }

    public void setHostName(View view) {
        EditText etHostName = findViewById(R.id.etHostName);
        if (etHostName.getText().toString().isEmpty()) return;
        LMHttpClient.BASE_URL = etHostName.getText().toString();
    }
}
