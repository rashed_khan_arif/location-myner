package mobile.location.locationminer;

public class LMLocation {
    private int id;
    private double lat;
    private double lng;
    private int userRelationId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getUserRelationId() {
        return userRelationId;
    }

    public void setUserRelationId(int userRelationId) {
        this.userRelationId = userRelationId;
    }
}
